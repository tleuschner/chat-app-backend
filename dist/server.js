"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express_1 = __importDefault(require("express"));
const socket_io_1 = __importDefault(require("socket.io"));
const http_1 = __importDefault(require("http"));
const router_1 = __importDefault(require("./router"));
const PORT = process.env.PORT || 3333;
const app = express_1.default();
const server = new http_1.default.Server(app);
const io = new socket_io_1.default.Server(server, {
    cors: {
        origin: "*",
    },
});
app.use(router_1.default);
io.on("connection", (socket) => {
    //user sent message, send to all others in the chat room
    socket.on("SEND_MESSAGE", (message) => {
        socket.broadcast.to(message.chatId).emit("RECEIVE_MESSAGE", message);
    });
    socket.on("join", ({ chatIds }) => {
        console.log("joinging", chatIds);
        socket.join(chatIds);
    });
    socket.on("disconnect", () => { });
});
server.listen(PORT, () => {
    console.log("server listening on port", PORT);
});
//# sourceMappingURL=server.js.map