require("dotenv").config();
import express from "express";
import socket, { Socket } from "socket.io";
import http from "http";
import router from "./router";
import { ChatMessageWithChatId, JoinRoomData } from "./interfaces";

const PORT = process.env.PORT || 3333;

const app = express();

const server = new http.Server(app);
const io = new socket.Server(server, {
  cors: {
    origin: "*",
  },
});

app.use(router);

io.on("connection", (socket: Socket) => {
  //user sent message, send to all others in the chat room
  socket.on("SEND_MESSAGE", (message: ChatMessageWithChatId) => {
    socket.broadcast.to(message.chatId).emit("RECEIVE_MESSAGE", message);
  });

  socket.on("join", ({ chatIds }: JoinRoomData) => {
    console.log("joinging", chatIds);
    socket.join(chatIds);
  });

  socket.on("disconnect", () => {});
});

server.listen(PORT, () => {
  console.log("server listening on port", PORT);
});
