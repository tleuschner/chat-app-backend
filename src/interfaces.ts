export interface ChatMessage {
  message: string;
  id: string;
  from: string;
  to: string;
  timestamp: number;
  name: string;
}

export interface ChatMessageWithChatId extends ChatMessage {
  chatId: string;
}

export interface JoinRoomData {
  userId: string;
  chatIds: string[];
}
